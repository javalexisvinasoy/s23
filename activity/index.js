console.log("Activity")

let trainer = {
	name: "Alexis Vinasoy",
	age: 22,
	pokemon: ["Pikachu", "Golem", "Onix"],
	friends: {
		name: "Natasha",
		name2: "Ralph"
	},
	talk: function() {
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer)
console.log("Result from dot notation: " + trainer.name)
trainer.talk()

function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 3 * level
	this.attack = 2 * level

	// Methods
	this.tackle = function(target) {

		if (target.health <= 5){
			console.log(target.name + " fainted")
		} else

		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
		target.health = target.health - this.attack
		console.log(target)
		
	},
	this.faint = function(){
		console.log(target.name + " fainted")
		
	}
	
}


let mewtwo = new Pokemon("Mewtwo", 7)
let nidoking = new Pokemon("Nidoking", 6)

console.log(mewtwo)
console.log(nidoking)

mewtwo.tackle(nidoking)
mewtwo.tackle(nidoking)
mewtwo.tackle(nidoking)

